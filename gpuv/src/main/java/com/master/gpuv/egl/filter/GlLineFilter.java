package com.master.gpuv.egl.filter;

import android.opengl.GLES20;
import android.util.Log;

import androidx.core.app.NotificationCompat;


/* loaded from: classes.dex */
public class GlLineFilter extends GlFilter {
    private static final String Line_FRAGMENT_SHADER = "precision mediump float; varying vec2 vTextureCoord;\n  \n uniform lowp sampler2D sTexture;\n  uniform highp float progress;\n  uniform highp float typeScan;\n  uniform highp float lineWidth;\n  \n  void main()\n  {\n      highp vec4 textureColor = texture2D(sTexture, vTextureCoord);\n      \n      gl_FragColor = textureColor;\n       if(progress != 1.0 && progress != 0.0){\n         if(typeScan==0.0 ||typeScan==2.0){\n              if(abs(progress - vTextureCoord.x)<lineWidth){\n                  gl_FragColor = vec4(0.22745,0.796,0.882353,1.0);\n              }\n          }else{\n              if(abs(progress - vTextureCoord.y)<lineWidth){\n                  gl_FragColor = vec4(0.22745,0.796,0.882353,1.0);\n              }\n          }\n       }\n  }\n";
    private float progress = 0.0f;
    private int typeScan = 0;
    private float lineWidth = 0.01f;
    private int width = 1;
    private int height = 1;

    public GlLineFilter() {
        super("attribute highp vec4 aPosition;\nattribute highp vec4 aTextureCoord;\nvarying highp vec2 vTextureCoord;\nvoid main() {\ngl_Position = aPosition;\nvTextureCoord = aTextureCoord.xy;\n}\n", Line_FRAGMENT_SHADER);
    }

    @Override // com.daasuu.gpuv.egl.filter.GlFilter
    public void setFrameSize(int i, int i2) {
        super.setFrameSize(i, i2);
        this.width = i;
        this.height = i2;
        updateLineWidth();
    }

    public void setProgress(float f) {
        this.progress = f;
    }

    private void updateLineWidth() {
        int i = this.typeScan;
        if (i == 0) {
            this.lineWidth = Utils.dpToPixel(2.0f) / this.width;
        } else {
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        return;
                    }
                }
                this.lineWidth = Utils.dpToPixel(2.0f) / this.width;
            }
            this.lineWidth = Utils.dpToPixel(2.0f) / this.height;
        }
//        this.lineWidth = Utils.dpToPixel(2.0f) / this.height;
//        this.lineWidth = Utils.dpToPixel(2.0f) / this.width;
//        this.lineWidth = Utils.dpToPixel(2.0f) / this.height;
    }

    public void setTypeScan(int i) {
        this.typeScan = i;
    }

    @Override // com.daasuu.gpuv.egl.filter.GlFilter
    public void onDraw() {
        GLES20.glUniform1f(getHandle(NotificationCompat.CATEGORY_PROGRESS), this.progress);
        GLES20.glUniform1f(getHandle("typeScan"), this.typeScan);
        GLES20.glUniform1f(getHandle("lineWidth"), this.lineWidth);
        Log.d("TAG", "onDraw: lineWidth "+lineWidth);
    }
}
