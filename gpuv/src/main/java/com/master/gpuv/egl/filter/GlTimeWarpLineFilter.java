package com.master.gpuv.egl.filter;

import android.os.Handler;
import android.os.Looper;

import com.master.gpuv.egl.GlFramebufferObject;

/* loaded from: classes.dex */
public class GlTimeWarpLineFilter extends GlFilter2 {
    private Callback callback;
    private GlFramebufferObject glFramebufferObject;
    private GlLineFilter glLineFilter;
    private GlTimeWarpFilter glTimeWarpFilter;
    private int type = 3;
    private float timeSpace = 5000.0f;
    private boolean isScan = true;
    private long lastTime = -1;
    private float progress = 0.0f;
    private boolean isCallbackSuccess = true;
    private Handler handler = new Handler(Looper.getMainLooper());

    @Override // com.daasuu.gpuv.egl.filter.GlFilter
    public void setup() {
        super.setup();
        this.glTimeWarpFilter = new GlTimeWarpFilter();
        this.glLineFilter = new GlLineFilter();
        this.glFramebufferObject = new GlFramebufferObject();
        this.glLineFilter.setup();
        this.glTimeWarpFilter.setup();
        setTypeScan(this.type);
    }

    @Override // com.daasuu.gpuv.egl.filter.GlFilter
    public void setFrameSize(int i, int i2) {
        super.setFrameSize(i, i2);
        GlFramebufferObject glFramebufferObject = this.glFramebufferObject;
        if (glFramebufferObject != null) {
            glFramebufferObject.setup(i, i2);
        }
        GlTimeWarpFilter glTimeWarpFilter = this.glTimeWarpFilter;
        if (glTimeWarpFilter != null) {
            glTimeWarpFilter.setFrameSize(i, i2);
        }
        GlLineFilter glLineFilter = this.glLineFilter;
        if (glLineFilter != null) {
            glLineFilter.setFrameSize(i, i2);
        }
    }

    public void onDraw(int i, GlFramebufferObject glFramebufferObject) {
        if (this.glTimeWarpFilter == null || this.glLineFilter == null || this.glFramebufferObject == null) {
            return;
        }
        updateProgressValue();
        float valueProgress = getValueProgress(this.progress);
        this.glTimeWarpFilter.setProgress(valueProgress);
        this.glLineFilter.setProgress(valueProgress);
        this.glFramebufferObject.enable();
        this.glTimeWarpFilter.draw(i, this.glFramebufferObject);
        glFramebufferObject.enable();
        this.glLineFilter.draw(this.glFramebufferObject.getTexName(), glFramebufferObject);
        updateProgressCallback();
    }

    private void updateProgressCallback() {
        if (this.callback != null) {
            this.handler.post(new Runnable() { // from class: com.daasuu.gpuv.egl.filter.GlTimeWarpLineFilter.1
                @Override // java.lang.Runnable
                public void run() {
                    GlTimeWarpLineFilter.this.callback.onUpdateProgress(GlTimeWarpLineFilter.this.progress);
                }
            });
            if (this.progress != 1.0f || !this.isCallbackSuccess) {
                return;
            }
            this.handler.post(new Runnable() { // from class: com.daasuu.gpuv.egl.filter.GlTimeWarpLineFilter.2
                @Override // java.lang.Runnable
                public void run() {
                    GlTimeWarpLineFilter.this.callback.onEnd();
                }
            });
            this.isCallbackSuccess = false;
        }
    }

    private void updateProgressValue() {
        if (this.isScan) {
            if (this.lastTime == -1) {
                this.lastTime = System.currentTimeMillis();
                this.progress = 0.0f;
            }
            float currentTimeMillis = ((float) (System.currentTimeMillis() - this.lastTime)) / this.timeSpace;
            this.progress = currentTimeMillis;
            if (currentTimeMillis <= 1.0f) {
                return;
            }
            this.progress = 1.0f;
        }
    }

    private float getValueProgress(float f) {
        int i = this.type;
        return (i == 1 || i == 2) ? 1.0f - f : f;
    }

    public void setTypeScan(int i) {
        this.type = i;
        GlLineFilter glLineFilter = this.glLineFilter;
        if (glLineFilter != null) {
            glLineFilter.setTypeScan(i);
        }
        GlTimeWarpFilter glTimeWarpFilter = this.glTimeWarpFilter;
        if (glTimeWarpFilter != null) {
            glTimeWarpFilter.setTypeScan(i);
        }
    }

    public boolean isScan() {
        return this.isScan;
    }

    @Override // com.daasuu.gpuv.egl.filter.GlFilter
    public void release() {
        GlTimeWarpFilter glTimeWarpFilter = this.glTimeWarpFilter;
        if (glTimeWarpFilter != null) {
            glTimeWarpFilter.release();
        }
        GlLineFilter glLineFilter = this.glLineFilter;
        if (glLineFilter != null) {
            glLineFilter.release();
        }
        GlFramebufferObject glFramebufferObject = this.glFramebufferObject;
        if (glFramebufferObject != null) {
            glFramebufferObject.release();
        }
        super.release();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void setDuration(int i) {
        this.timeSpace = i;
    }

    public void start() {
        this.isScan = true;
        this.isCallbackSuccess = true;
        if (this.callback != null) {
            this.handler.post(new Runnable() { // from class: com.daasuu.gpuv.egl.filter.GlTimeWarpLineFilter.3
                @Override // java.lang.Runnable
                public void run() {
                    GlTimeWarpLineFilter.this.callback.onStart();
                }
            });
        }
    }

    public void pause() {
        this.isScan = false;
    }

    public void reset() {
        this.lastTime = -1L;
        this.progress = 0.0f;
        this.isScan = false;
    }

    /* loaded from: classes.dex */
    public interface Callback {
        void onEnd();

        void onStart();

        void onUpdateProgress(float f);
    }
}
