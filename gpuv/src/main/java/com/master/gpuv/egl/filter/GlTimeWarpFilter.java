package com.master.gpuv.egl.filter;

import android.opengl.GLES20;

import androidx.core.app.NotificationCompat;

import com.master.gpuv.egl.EglUtil;
import com.master.gpuv.egl.GlFramebufferObject;

/* loaded from: classes.dex */
public class GlTimeWarpFilter extends GlFilter2 {
    public static final int BOTTOM_TO_TOP = 3;
    public static final int LEFT_TO_RIGHT = 0;
    public static final int RIGHT_TO_LEFT = 2;
    public static final int TOP_TO_BOTTOM = 1;
    private static final String RGB_FRAGMENT_SHADER = "precision mediump float; varying vec2 vTextureCoord;\n  \n uniform lowp sampler2D sTexture;\n uniform lowp sampler2D uTexture;\n uniform lowp float progress;\n uniform lowp float typeScan;\n  void main()\n  {\n       highp vec4 textureColor = texture2D(sTexture, vTextureCoord);\n       highp vec4 texture = texture2D(uTexture, vTextureCoord);\n       if(typeScan==0.0){\n           if(vTextureCoord.x > progress){\n               gl_FragColor = textureColor;\n           }else{\n               gl_FragColor = texture;\n           }\n       }else if(typeScan==1.0){\n           if(vTextureCoord.y > progress){\n               gl_FragColor = texture;\n           }else{\n               gl_FragColor = textureColor;\n           }\n       }else if(typeScan==2.0){\n           if(vTextureCoord.x > progress){\n               gl_FragColor = texture;\n           }else{\n               gl_FragColor = textureColor;\n           }\n       }else if(typeScan==3.0){\n           if(vTextureCoord.y > progress){\n               gl_FragColor = textureColor;\n           }else{\n               gl_FragColor = texture;\n           }\n       }\n  }\n";
    private GlFramebufferObject framebufferObject;
    private float progress = 0.0f;
    private int type = 3;

    public GlTimeWarpFilter() {
        super("attribute highp vec4 aPosition;\nattribute highp vec4 aTextureCoord;\nvarying highp vec2 vTextureCoord;\nvoid main() {\ngl_Position = aPosition;\nvTextureCoord = aTextureCoord.xy;\n}\n", RGB_FRAGMENT_SHADER);
    }

    @Override // com.daasuu.gpuv.egl.filter.GlFilter
    public void onDraw() {
    }

    @Override // com.daasuu.gpuv.egl.filter.GlFilter
    public void setup() {
        super.setup();
        this.framebufferObject = new GlFramebufferObject();
        getHandle("uTexture");
        getHandle(NotificationCompat.CATEGORY_PROGRESS);
        getHandle("typeScan");
    }

    @Override // com.daasuu.gpuv.egl.filter.GlFilter
    public void setFrameSize(int i, int i2) {
        GlFramebufferObject glFramebufferObject = this.framebufferObject;
        if (glFramebufferObject != null) {
            glFramebufferObject.setup(i, i2);
        }
    }

    @Override // com.daasuu.gpuv.egl.filter.GlFilter
    public void draw(int i, GlFramebufferObject glFramebufferObject) {
        GlFramebufferObject glFramebufferObject2 = this.framebufferObject;
        if (glFramebufferObject2 != null) {
            glFramebufferObject2.enable();
            scan(i, this.framebufferObject, this.progress);
            glFramebufferObject.enable();
            scan(i, this.framebufferObject, this.progress);
        }
    }

    private void scan(int i, GlFramebufferObject glFramebufferObject, float f) {
        useProgram();
        GLES20.glBindBuffer(34962, this.vertexBufferName);
        GLES20.glEnableVertexAttribArray(getHandle("aPosition"));
        GLES20.glVertexAttribPointer(getHandle("aPosition"), 3, 5126, false, 20, 0);
        GLES20.glEnableVertexAttribArray(getHandle("aTextureCoord"));
        GLES20.glVertexAttribPointer(getHandle("aTextureCoord"), 2, 5126, false, 20, 12);
        GLES20.glUniform1f(getHandle(NotificationCompat.CATEGORY_PROGRESS), f);
        GLES20.glUniform1f(getHandle("typeScan"), this.type);
        activateTexture(i, 0, getHandle(GlFilter.DEFAULT_UNIFORM_SAMPLER));
        activateTexture(glFramebufferObject.getTexName(), 1, getHandle("uTexture"));
        EglUtil.setupSampler(3553, 9728, 9728);
        GLES20.glDrawArrays(5, 0, 4);
    }

    private void activateTexture(int i, int i2, int i3) {
        GLES20.glActiveTexture(33984 + i2);
        GLES20.glBindTexture(3553, i);
        GLES20.glUniform1i(i3, i2);
    }

    public void setTypeScan(int i) {
        this.type = i;
    }

    public void setProgress(float f) {
        this.progress = f;
    }
}
