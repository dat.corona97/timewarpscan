package com.master.gpuv.egl.filter;

import android.content.res.Resources;
import android.util.TypedValue;

public class Utils {
    public static float dpToPixel(float f) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, f, Resources.getSystem().getDisplayMetrics());
    }
}
