package com.master.gpuv.camerarecorder;


public enum CaptureMode {
    PHOTO(0),
    VIDEO(1);

    private int mode;

    CaptureMode(int facing) {
        this.mode = facing;
    }

    public int getMode() {
        return mode;
    }
}
