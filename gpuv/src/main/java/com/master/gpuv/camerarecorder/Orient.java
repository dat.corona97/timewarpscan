package com.master.gpuv.camerarecorder;


public enum Orient {
    VERTICAL(0),
    HORIZONTAL(1);

    private int orient;

    Orient(int facing) {
        this.orient = facing;
    }

    public int getOrient() {
        return orient;
    }
}
