package com.master.timewarp;

import android.app.Application;

import com.google.android.exoplayer2.database.ExoDatabaseProvider;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.master.timewarp.database.RoomDatabase;

public class TimeWarpApplication extends Application {
    private static TimeWarpApplication instance;
    private RoomDatabase roomDatabase;
    private SimpleCache cache;

    public SimpleCache getCache() {
        return cache;
    }

    public static TimeWarpApplication getInstance() {
        return instance;
    }

    public RoomDatabase getRoomDatabase() {
        return roomDatabase;
    }

    private long cacheSize = 90 * 1024 * 1024;
    private LeastRecentlyUsedCacheEvictor cacheEvictor;
    private ExoDatabaseProvider exoplayerDatabaseProvider;


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        roomDatabase = RoomDatabase.getDatabase(this);

        cacheEvictor = new LeastRecentlyUsedCacheEvictor(cacheSize);
        exoplayerDatabaseProvider = new ExoDatabaseProvider(this);
        cache = new SimpleCache(getCacheDir(), cacheEvictor, exoplayerDatabaseProvider);

    }
}
