package com.master.timewarp.model;

public class ItemLanguage {
    private int imageFlag;
    private String name;
    private int imgSelect;
    private String languageToLoad;

    public ItemLanguage(int imageFlag, String name, int imgSelect, String languageToLoad) {
        this.imageFlag = imageFlag;
        this.name = name;
        this.imgSelect = imgSelect;
        this.languageToLoad = languageToLoad;
    }

    public String getLanguageToLoad() {
        return languageToLoad;
    }

    public void setLanguageToLoad(String languageToLoad) {
        this.languageToLoad = languageToLoad;
    }

    public int getImageFlag() {
        return imageFlag;
    }

    public void setImageFlag(int imageFlag) {
        this.imageFlag = imageFlag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImgSelect() {
        return imgSelect;
    }

    public void setImgSelect(int imgSelect) {
        this.imgSelect = imgSelect;
    }
}
