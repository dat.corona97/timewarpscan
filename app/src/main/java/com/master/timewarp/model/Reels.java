package com.master.timewarp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Reels {
    @SerializedName("reels")
    @Expose
    private List<String> reels = null;

    public List<String> getReels() {
        return reels;
    }

    public void setReels(List<String> reels) {
        this.reels = reels;
    }
}
