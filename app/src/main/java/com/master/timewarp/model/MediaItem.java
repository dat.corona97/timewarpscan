package com.master.timewarp.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "gallery_table")
public class MediaItem implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String path;
    private int type;

    private String author;
    private String title;
    private String youtubeID;


    public String getYoutubeID() {
        return youtubeID;
    }

    public void setYoutubeID(String youtubeID) {
        this.youtubeID = youtubeID;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MediaItem() {
    }

    public MediaItem(String url, int type) {
        this.path = url;
        this.type = type;
    }

    public String getYoutubeUrl() {
        return "https://www.youtube.com/watch?v=" + youtubeID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

}
