package com.master.timewarp.model;

import java.io.Serializable;

public class MediaSource implements Serializable {
    private String id, author, title, youtubeID;

    public MediaSource(String id, String author, String title, String youtubeID) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.youtubeID = youtubeID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYoutubeID() {
        return youtubeID;
    }
}
