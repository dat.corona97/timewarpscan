package com.master.timewarp.base;

import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;


abstract public class BaseViewHolder<T extends ViewDataBinding> extends RecyclerView.ViewHolder {
    protected T binding;

    public BaseViewHolder(T viewBinding) {
        super(viewBinding.getRoot());
        binding = viewBinding;
    }

    abstract public void loadData(Object data);
}
