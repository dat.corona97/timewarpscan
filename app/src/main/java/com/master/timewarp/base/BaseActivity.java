package com.master.timewarp.base;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;

public abstract class BaseActivity<T extends ViewDataBinding> extends LocalizationActivity {
    protected T binding;
    protected Context context;

    @Override
    protected void attachBaseContext(@NonNull Context newBase) {
        super.attachBaseContext(newBase);
        this.context = newBase;
    }


    @Override
    protected final void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getLayoutId());
        binding.executePendingBindings();
        initView();
        addAction();
    }


    protected abstract int getLayoutId();

    protected abstract void initView();

    protected abstract void addAction();


}
