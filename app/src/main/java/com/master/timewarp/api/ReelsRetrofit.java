package com.master.timewarp.api;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class ReelsRetrofit {
    private static Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("http://mjinfotech.tech:3000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    public static TimeWarpApi getTimeWarpApi() {
        return getRetrofit().create(TimeWarpApi.class);
    }
}
