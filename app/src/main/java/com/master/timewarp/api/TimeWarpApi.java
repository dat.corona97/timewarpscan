package com.master.timewarp.api;

import com.master.timewarp.model.Reels;

import retrofit.Call;
import retrofit.http.GET;

public interface TimeWarpApi {
    @GET("/reels")
    Call<Reels> getReels();

}
