package com.master.timewarp.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;

import com.master.timewarp.database.dao.GalleryDao;
import com.master.timewarp.model.MediaItem;

@Database(entities = {MediaItem.class}, version = 3, exportSchema = false)
public abstract class RoomDatabase extends androidx.room.RoomDatabase {
    private static RoomDatabase instance;

    public static RoomDatabase getDatabase(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, RoomDatabase.class, "timewarp_db").allowMainThreadQueries()
                    .fallbackToDestructiveMigration().build();
        }
        return instance;
    }

    public abstract GalleryDao galleryDao();

}
