package com.master.timewarp.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.master.timewarp.model.MediaItem;

import java.util.List;

@Dao
public interface GalleryDao {
    @Query("SELECT * FROM gallery_table ORDER BY id DESC")
    List<MediaItem> getList();

    @Query("SELECT * FROM gallery_table WHERE path =:path ")
    MediaItem getObject(String path);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void add(MediaItem mediaItem);

    @Update
    void update(MediaItem mediaItem);

    @Query("DELETE FROM gallery_table WHERE id =:id ")
    void delete(int id);

    @Query("DELETE FROM gallery_table WHERE path =:path ")
    void delete(String path);

    @Query("DELETE FROM gallery_table")
    void deleteAll();
}
