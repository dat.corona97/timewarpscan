package com.master.timewarp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.view.View;

import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;
import androidx.viewpager.widget.ViewPager;

import com.master.timewarp.R;
import com.master.timewarp.TimeWarpApplication;
import com.master.timewarp.base.BaseActivity;
import com.master.timewarp.databinding.ActivityPreviewBinding;
import com.master.timewarp.model.MediaItem;
import com.master.timewarp.view.adapter.SlidePagerAdapter;
import com.master.timewarp.view.dialog.DeleteFileDialog;
import com.master.timewarp.view.dialog.DiscardFileDialog;
import com.master.timewarp.view.dialog.PermissionDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class PreviewActivity extends BaseActivity<ActivityPreviewBinding> {
    private boolean fromMain;
    private boolean fromSave;
    private boolean fromScan;
    private List<MediaItem> list;
    private MediaItem currentMediaItem;
    private int index;
    private SlidePagerAdapter slideAdapter;

    public static void startFromMain(Context context, List<MediaItem> list, int index) {
        start(context, list, index, true, false, false);
    }

    public static void startFromCamera(Context context, MediaItem currentMediaItem) {
        start(context, Collections.singletonList(currentMediaItem), 0, true, false, false);
    }

    public static void startFromSave(Context context, MediaItem currentMediaItem) {
        start(context, Collections.singletonList(currentMediaItem), 0, false, true, false);
    }

    public static void startFromScan(Context context, MediaItem currentMediaItem) {
        start(context, Collections.singletonList(currentMediaItem), 0, false, false, true);
    }

    private static void start(Context context, List<MediaItem> currentMediaItems, int index, boolean fromMain, boolean fromSave, boolean fromScan) {
        Intent starter = new Intent(context, PreviewActivity.class);
        starter.putExtra("list", (Serializable) currentMediaItems);
        starter.putExtra("index", index);
        starter.putExtra("fromMain", fromMain);
        starter.putExtra("fromSave", fromSave);
        starter.putExtra("fromScan", fromScan);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_preview;
    }

    @Override
    protected void initView() {
        fromMain = getIntent().getBooleanExtra("fromMain", false);
        fromSave = getIntent().getBooleanExtra("fromSave", false);
        fromScan = getIntent().getBooleanExtra("fromScan", false);

        list = (List<MediaItem>) getIntent().getSerializableExtra("list");
        index = getIntent().getIntExtra("index", 0);
        currentMediaItem = list.get(index);

        slideAdapter = new SlidePagerAdapter(list, getSupportFragmentManager());
        binding.viewPager.setAdapter(slideAdapter);
        binding.viewPager.setCurrentItem(index);

        if (fromMain) {
            binding.btShare.setVisibility(View.VISIBLE);
            binding.btDelete.setVisibility(View.VISIBLE);
            binding.tvTitle.setText(currentMediaItem.getType() == 1 ? getString(R.string.video) : getString(R.string.image));
        }
        if (fromSave) {
            binding.btShare.setVisibility(View.VISIBLE);
            binding.btScanMore.setVisibility(View.VISIBLE);
            String type = currentMediaItem.getType() == 1 ? "video" : "image";
            showToast(String.format("Your %s has been saved to gallery", type));
        }
        if (fromScan) {
            binding.btSave.setVisibility(View.VISIBLE);
        }
    }

    private void initSlide() {

    }

    private void showToast(String message) {
        binding.frToast.setVisibility(View.VISIBLE);
        binding.layoutToast.tvMessage.setText(message);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                binding.frToast.setVisibility(View.GONE);
            }
        }, 2000);

    }


    @Override
    protected void addAction() {
        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentMediaItem = list.get(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        binding.btBack.setOnClickListener(v -> onBackPressed());
        binding.btSave.setOnClickListener(v -> saveMedia());
        binding.btScanMore.setOnClickListener(v -> {
            CameraScannerActivity.startWithTrending(this);
            finish();
        });

        binding.btShare.setOnClickListener(v -> shareFile(new File(currentMediaItem.getPath())));
        binding.btDelete.setOnClickListener(v -> showDialogDelete());

        binding.btPremium.setOnClickListener(v -> PremiumActivity.start(this));

    }

    private void saveMedia() {
        PermissionDialog.newInstance().setCallback(new PermissionDialog.PermissionCallback() {
            @Override
            public void onPermissionGranted() {

                startFromSave(PreviewActivity.this, currentMediaItem);
                copyFile(new File(currentMediaItem.getPath()), new File(currentMediaItem.getType() == 0 ? getImageFilePath() : getVideoFilePath()));
                TimeWarpApplication.getInstance().getRoomDatabase().galleryDao().add(currentMediaItem);
                finish();
            }

            @Override
            public void onPermissionDenied() {

            }
        }).setStoragePermission().showDialog(this);

    }

    public void shareFile(File file) {
        try {
            Uri uri = FileProvider.getUriForFile(this, this.getPackageName() + ".provider", file);
            try {
                Intent intent = ShareCompat.IntentBuilder.from(this).setType(this.getContentResolver().getType(uri)).setStream(uri).getIntent();
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                Intent createChooser = Intent.createChooser(intent, "Share File");
                createChooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (createChooser.resolveActivity(getPackageManager()) == null) {
                    return;
                }
                startActivity(createChooser);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        if (fromMain) {
            finish();
            return;
        }
        if (fromScan) {
            showDiscard();
            return;
        }
        CameraScannerActivity.start(this);
        finish();
    }

    private void showDiscard() {
        DiscardFileDialog discardFileDialog = DiscardFileDialog.newInstance(currentMediaItem.getType());
        discardFileDialog.setCallback(new DiscardFileDialog.ICallback() {
            @Override
            public void onSave() {
//                saveMedia();
            }

            @Override
            public void onDiscard() {
                deleteMedia();
            }
        });
        discardFileDialog.showDialog(this);
    }

    private void deleteMedia() {
        new File(currentMediaItem.getPath()).delete();
        list.remove(currentMediaItem);
        slideAdapter.updateList(list);
        index--;
        binding.viewPager.setAdapter(slideAdapter);
        binding.viewPager.setCurrentItem(index);
        try {
            currentMediaItem=list.get(index);
        } catch (Exception e) {
            e.printStackTrace();
            index=0;
            try {
                currentMediaItem=list.get(0);
                binding.viewPager.setCurrentItem(index);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        if (list.isEmpty() && fromMain) {
            finish();
            return;
        }
        if (!fromMain) {
            CameraScannerActivity.start(PreviewActivity.this);
            finish();
        }
    }

    private void showDialogDelete() {
        DeleteFileDialog deleteFileDialog = DeleteFileDialog.newInstance(currentMediaItem.getType());
        deleteFileDialog.setCallback(new DeleteFileDialog.ICallback() {
            @Override
            public void onDelete() {
                deleteMedia();
            }


            @Override
            public void onCancel() {
                deleteFileDialog.dismiss();
            }
        });
        deleteFileDialog.show(getSupportFragmentManager(), "");
    }

    public static String getImageFilePath() {
        return getAndroidImageFolder().getAbsolutePath() + "/" + new SimpleDateFormat("yyyyMM_dd-HHmmss").format(new Date()) + "TimeWarpScan.png";
    }

    public static String getVideoFilePath() {
        return getAndroidVideoFolder().getAbsolutePath() + "/" + new SimpleDateFormat("yyyyMM_dd-HHmmss").format(new Date()) + "TimeWarpScan.mp4";
    }

    public static void copyFile(File sourceLocation, File targetLocation) {
        try {
            InputStream in = new FileInputStream(sourceLocation);
            FileOutputStream out = new FileOutputStream(targetLocation);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static File getAndroidImageFolder() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
    }

    public static File getAndroidVideoFolder() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
    }
}
