package com.master.timewarp.view.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;

import com.bumptech.glide.Glide;
import com.master.timewarp.R;
import com.master.timewarp.base.BaseFragment;
import com.master.timewarp.databinding.FragmentSlideBinding;
import com.master.timewarp.model.MediaItem;
import com.master.timewarp.view.activity.PreviewFullScreenActivity;

public class SlideFragment extends BaseFragment<FragmentSlideBinding> {
    private MediaItem mediaItem;

    public static SlideFragment newInstance(MediaItem mediaItem) {
        Bundle args = new Bundle();
        args.putSerializable("data", mediaItem);
        SlideFragment fragment = new SlideFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_slide;
    }

    @Override
    protected void initView() {
        mediaItem = (MediaItem) getArguments().getSerializable("data");
        try {
            Glide.with(this).load(mediaItem.getPath()).into(binding.ivThumb);
        } catch (Exception e) {
            e.printStackTrace();
        }
        binding.ivPlayIcon.setVisibility(mediaItem.getType() == 1 ? View.VISIBLE : View.GONE);

    }

    @Override
    protected void addAction() {
        binding.frThumb.setOnClickListener(v -> {
            if (mediaItem.getType() == 1) {
                playVideo();
            }
        });
        binding.btFullscreen.setOnClickListener(v -> {
            PreviewFullScreenActivity.start(context, mediaItem);
        });
    }

    private void playVideo() {
        binding.frThumb.setVisibility(View.GONE);
        binding.videoView.setOnPreparedListener(mediaPlayer -> {
            mediaPlayer.setLooping(true);
            float videoWidth = (((float) mediaPlayer.getVideoWidth()) / ((float) mediaPlayer.getVideoHeight())) / (((float) binding.videoView.getWidth()) / ((float) binding.videoView.getHeight()));
            if (videoWidth >= 1.0f) {
                binding.videoView.setScaleX(videoWidth);
            } else {
                binding.videoView.setScaleY(1.0f / videoWidth);
            }
        });
        binding.videoView.setMediaController(new MediaController(context));
        binding.videoView.setVideoPath(mediaItem.getPath());
        binding.videoView.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.frThumb.setVisibility(View.VISIBLE);
    }

}
