package com.master.timewarp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.viewpager.widget.ViewPager;

import com.master.timewarp.R;
import com.master.timewarp.view.adapter.TrendingVerticalPagerAdapter;
import com.master.timewarp.base.BaseActivity;
import com.master.timewarp.databinding.ActivityPreviewTrendingBinding;
import com.master.timewarp.model.MediaItem;
import com.master.timewarp.view.dialog.PermissionDialog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PreviewTrendingActivity extends BaseActivity<ActivityPreviewTrendingBinding> {
    public static final String ACTION_PLAY_STOP = "action_play_stop";
    private TrendingVerticalPagerAdapter viewPagerAdapter;
    private int index;
    private List<MediaItem> list;
    private Handler handle;

    public static void start(Context context, List<MediaItem> list, int index) {
        Intent starter = new Intent(context, PreviewTrendingActivity.class);
        starter.putExtra("data", (Serializable) list);
        starter.putExtra("index", index);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_preview_trending;
    }

    @Override
    protected void initView() {
        list = (List<MediaItem>) getIntent().getSerializableExtra("data");
        index = getIntent().getIntExtra("index", 0);
        Log.d("TAG", "initView: ");

        viewPagerAdapter = new TrendingVerticalPagerAdapter(new ArrayList<>(), getSupportFragmentManager());

        loadMedia(list.get(index));

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                Intent intent = new Intent(ACTION_PLAY_STOP);
//                intent.putExtra("index", index);
//                sendBroadcast(intent);
//            }
//        }, 1000);
    }

    private void loadMedia(MediaItem mediaItem) {
        List<MediaItem> temp = new ArrayList<>();
        temp.add(null);
        temp.add(mediaItem);
        temp.add(null);
        binding.viewPager.setOnPageChangeListener(null);
        viewPagerAdapter.updateList(temp);
        binding.viewPager.setAdapter(viewPagerAdapter);
        binding.viewPager.setOffscreenPageLimit(3);
        binding.viewPager.setCurrentItem(1);
        new Handler().postDelayed(() -> {
            binding.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    Log.d("TAG", "onPageScrolled: " + position);
                }

                @Override
                public void onPageSelected(int position) {
                    Log.d("TAG", "onPageSelected: " + position);
                    if (position == 0) {
                        if (index > 0) {
                            index--;
                        }
                    } else if (position == 2) {
                        if (index < list.size()) {
                            index++;
                        }
                    }
                    if (position != 1) {
                        binding.viewPager.setPagingEnabled(false);
                        if (handle != null) {
                            handle.removeCallbacksAndMessages(null);
                        }
                        handle = new Handler();
                        handle.postDelayed(() -> {
                            try {
                                loadMedia(list.get(index));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }, 500L);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            binding.viewPager.setPagingEnabled(true);
        }, 200L);
    }


    @Override
    protected void addAction() {
        binding.btClose.setOnClickListener(v -> onBackPressed());
        binding.btPremium.setOnClickListener(view -> PremiumActivity.start(this));
        binding.btTryNow.setOnClickListener(v -> {
            PermissionDialog.newInstance().setCallback(new PermissionDialog.PermissionCallback() {
                @Override
                public void onPermissionGranted() {
                    CameraScannerActivity.start(PreviewTrendingActivity.this);
                    finish();
                }

                @Override
                public void onPermissionDenied() {
                    Toast.makeText(PreviewTrendingActivity.this, "Permission is denied!", Toast.LENGTH_SHORT).show();
                }
            }).showDialog(this);
        });

    }
}
