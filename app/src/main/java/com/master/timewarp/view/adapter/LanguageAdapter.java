package com.master.timewarp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.master.timewarp.base.BaseAdapter;
import com.master.timewarp.databinding.ItemLanguageBinding;
import com.master.timewarp.model.ItemLanguage;

import java.util.List;

public class LanguageAdapter extends BaseAdapter<ItemLanguage> {
    public LanguageAdapter(List<ItemLanguage> mList, Context context) {
        super(mList, context);
    }
    private LanguageCallback languageCallback;

    public void setLanguageCallback(LanguageCallback languageCallback) {
        this.languageCallback = languageCallback;
    }

    @Override
    protected RecyclerView.ViewHolder viewHolder(ViewGroup parent, int viewType) {
        ItemLanguageBinding binding = ItemLanguageBinding.inflate(LayoutInflater.from(context), parent, false);
        return new LanguageViewHolder(binding);
    }

    @Override
    protected void onBindView(RecyclerView.ViewHolder viewHolder, int position) {
        ItemLanguage itemLanguage = mList.get(position);
        if (viewHolder instanceof LanguageViewHolder) {
            LanguageViewHolder holder = (LanguageViewHolder) viewHolder;
            holder.loadData(itemLanguage);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    private class LanguageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ItemLanguageBinding binding;

        public LanguageViewHolder(ItemLanguageBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            itemView.setOnClickListener(this);
        }

        public void loadData(ItemLanguage itemLanguage) {
            itemView.setTag(itemLanguage);
//            binding.imgFlag.setImageResource(itemLanguage.getImageFlag());
            binding.tvLanguage.setText(itemLanguage.getName());
            binding.rbCheck.setImageResource(itemLanguage.getImgSelect());
        }

        @Override
        public void onClick(View v) {
            languageCallback.clickItem((ItemLanguage) itemView.getTag());
        }
    }
    public interface LanguageCallback{
        void clickItem(ItemLanguage itemLanguage);
    }
}
