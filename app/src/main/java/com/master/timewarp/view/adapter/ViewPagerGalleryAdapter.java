package com.master.timewarp.view.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.master.timewarp.view.fragment.GalleryMediaFragment;

public class ViewPagerGalleryAdapter extends FragmentStatePagerAdapter {

    public ViewPagerGalleryAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return GalleryMediaFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return position == 0 ? "Trending Videos" : "Gallery";
    }
}
