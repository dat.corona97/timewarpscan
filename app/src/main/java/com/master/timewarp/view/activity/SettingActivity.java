package com.master.timewarp.view.activity;

import android.content.Context;
import android.content.Intent;

import com.master.timewarp.R;
import com.master.timewarp.base.BaseActivity;
import com.master.timewarp.databinding.ActivitySettingBinding;
import com.master.timewarp.view.dialog.LanguageDialog;

public class SettingActivity extends BaseActivity<ActivitySettingBinding> {
    public static void start(Context context) {
        Intent starter = new Intent(context, SettingActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_setting;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void addAction() {
        binding.btClose.setOnClickListener(v -> {
            finish();
        });

        binding.btPremium.setOnClickListener(v -> {
            PremiumActivity.start(this);
        });
        binding.btLanguage.setOnClickListener(v -> {
            LanguageDialog dialog = LanguageDialog.newInstance();
            dialog.setCallback(itemLanguage -> {
//                finishAffinity();
//                startActivity(new Intent(this, SplashActivity.class));
                setLanguage(itemLanguage.getLanguageToLoad());
            });
            dialog.show(getSupportFragmentManager(), "");

        });
        binding.btShareApp.setOnClickListener(v -> {

        });
        binding.btPolicy.setOnClickListener(v -> {

        });
        binding.btRateApp.setOnClickListener(v -> {

        });


    }
}
