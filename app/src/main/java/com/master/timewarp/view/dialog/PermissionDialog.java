package com.master.timewarp.view.dialog;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;

import com.master.timewarp.R;
import com.master.timewarp.base.BaseDialog;
import com.master.timewarp.databinding.DialogPermissionBinding;
import com.master.timewarp.utils.SharePrefUtil;

import java.util.Objects;


public class PermissionDialog extends BaseDialog<DialogPermissionBinding> {

    private static String[] PERMISSIONS = new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA};

    private static final int RQC_REQUEST_PERMISSION = 1002;
    private PermissionCallback callback;


    public static PermissionDialog newInstance() {
        Bundle args = new Bundle();
        PermissionDialog fragment = new PermissionDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static boolean isPermissionGranted(Activity context) {
        return !(context.checkSelfPermission(PERMISSIONS[0]) != PackageManager.PERMISSION_GRANTED
                || context.checkSelfPermission(PERMISSIONS[1]) != PackageManager.PERMISSION_GRANTED);
    }

    public PermissionDialog setCallback(PermissionCallback callback) {
        this.callback = callback;
        return this;
    }

    public PermissionDialog setStoragePermission() {
        PERMISSIONS = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        return this;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_permission;
    }

    @Override
    protected void initView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isPermissionGranted((Activity) getContext())) {
                if (callback != null) {
                    callback.onPermissionGranted();
                }
                dismiss();
                return;
            }
        } else {
            if (callback != null) {
                callback.onPermissionGranted();
            }
            dismiss();
            return;
        }
        if (SharePrefUtil.getInstance().isPermissionDeny() && isRationale()) {
            showDefaultPermissionDialog(getContext());
            dismiss();
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(PERMISSIONS, RQC_REQUEST_PERMISSION);
        }

    }

    private boolean isRationale() {
        for (String permission : PERMISSIONS) {
            if (!shouldShowRequestPermissionRationale(permission)) {
                return true;
            }
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void showDefaultPermissionDialog(Context context) {
        String msg;
        if (Objects.equals(PERMISSIONS[0], Manifest.permission.READ_EXTERNAL_STORAGE)) {
            msg = getString(R.string.pl_grant_permission_storage_desc);
        } else {
            msg = getString(R.string.pl_grant_permission_normal_desc);
        }
        new AlertDialog.Builder(context)
                .setTitle(R.string.pl_grant_permission)
                .setMessage(msg)
                .setPositiveButton(getString(R.string.go_setting), (dialog, which) -> {
                    final Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    intent.setData(Uri.parse("package:" + context.getPackageName()));
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_USER_ACTION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    context.startActivity(intent);
                    dialog.dismiss();
                }).setNegativeButton(R.string.cancel, (dialog, which) -> dialog.cancel())
                .show();
    }

    @Override
    protected void addAction() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (callback == null) {
            dismiss();
            return;
        }
        if (requestCode == RQC_REQUEST_PERMISSION) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isPermissionGranted((Activity) getContext())) {
                    callback.onPermissionGranted();
                } else {
                    callback.onPermissionDenied();
                }
            } else {
                callback.onPermissionGranted();
            }
            dismiss();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (callback == null) {
            dismiss();
            return;
        }
        if (requestCode == RQC_REQUEST_PERMISSION) {
            if (isPermissionGranted(getActivity())) {
                if (callback != null) {
                    callback.onPermissionGranted();
                }
            } else {
                if (callback != null) {
                    callback.onPermissionDenied();
                }
                SharePrefUtil.getInstance().setDenyPermission(true);
            }
            dismiss();
        }
    }

    public interface PermissionCallback {
        void onPermissionGranted();

        void onPermissionDenied();
    }
}
