package com.master.timewarp.view.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.master.timewarp.model.MediaItem;
import com.master.timewarp.view.fragment.BlankFragment;
import com.master.timewarp.view.fragment.PreviewTrendingFragment;

import java.util.List;

public class TrendingVerticalPagerAdapter extends FragmentStatePagerAdapter {
    private List<MediaItem> list;

    public TrendingVerticalPagerAdapter(List<MediaItem> list, @NonNull FragmentManager fm) {
        super(fm);
        this.list = list;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return list.get(position) == null ? BlankFragment.newInstance() : PreviewTrendingFragment.newInstance(list.get(position), position);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    public void updateList(List<MediaItem> temp) {
        this.list=temp;
        notifyDataSetChanged();
    }
}
