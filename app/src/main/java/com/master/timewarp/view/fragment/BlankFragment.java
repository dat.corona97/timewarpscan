package com.master.timewarp.view.fragment;

import android.os.Bundle;

import com.master.timewarp.R;
import com.master.timewarp.base.BaseFragment;
import com.master.timewarp.databinding.FragmentBlankBinding;

public class BlankFragment extends BaseFragment<FragmentBlankBinding> {
    public static BlankFragment newInstance() {
        
        Bundle args = new Bundle();
        
        BlankFragment fragment = new BlankFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_blank;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void addAction() {

    }
}
