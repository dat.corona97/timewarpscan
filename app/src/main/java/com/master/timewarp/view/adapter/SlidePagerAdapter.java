package com.master.timewarp.view.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.master.timewarp.model.MediaItem;
import com.master.timewarp.view.fragment.SlideFragment;

import java.util.ArrayList;
import java.util.List;

public class SlidePagerAdapter extends FragmentStatePagerAdapter {
    private List<MediaItem> list=new ArrayList<>();


    public SlidePagerAdapter(List<MediaItem> list, @NonNull FragmentManager fm) {
        super(fm);
        this.list = list;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return SlideFragment.newInstance(list.get(position));
    }

    @Override
    public int getCount() {
        return list.size();
    }


    public void updateList(List<MediaItem> list) {
        this.list=list;

    }
}
