package com.master.timewarp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.master.timewarp.base.BaseAdapter;
import com.master.timewarp.base.BaseViewHolder;
import com.master.timewarp.databinding.ItemMediaBinding;
import com.master.timewarp.model.MediaItem;
import com.master.timewarp.view.activity.PreviewActivity;

import java.util.List;

public class MediaItemAdapter extends BaseAdapter<MediaItem> {
    private ICallback callback;

    public MediaItemAdapter(List<MediaItem> mList, Context context) {
        super(mList, context);
    }

    public void setCallback(ICallback callback) {
        this.callback = callback;
    }

    @Override
    protected RecyclerView.ViewHolder viewHolder(ViewGroup parent, int viewType) {
        return new MediaItemViewHolder(ItemMediaBinding.inflate(LayoutInflater.from(context), parent, false));
    }

    @Override
    protected void onBindView(RecyclerView.ViewHolder viewHolder, int position) {
        MediaItemViewHolder holder = (MediaItemViewHolder) viewHolder;
        holder.loadData(mList.get(position));
    }

    public void updateList(List<MediaItem> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    public interface ICallback {
        void onClickItem(MediaItem mediaItem);
    }

    private class MediaItemViewHolder extends BaseViewHolder<ItemMediaBinding> {

        public MediaItemViewHolder(ItemMediaBinding viewBinding) {
            super(viewBinding);
            binding.root.setOnClickListener(v -> {

                MediaItem mediaItem = (MediaItem) itemView.getTag();
                if (callback != null) {
                    callback.onClickItem(mediaItem);
                } else {
                    PreviewActivity.startFromMain(context, mList, mList.indexOf(mediaItem));
                }
            });
        }

        @Override
        public void loadData(Object data) {
            itemView.setTag(data);
            MediaItem item = (MediaItem) data;
            binding.ivPlayIcon.setVisibility(item.getType() == 1 ? View.VISIBLE : View.GONE);
            try {
                Glide.with(context).load(item.getPath()).into(binding.ivThumb);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
