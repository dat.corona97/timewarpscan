package com.master.timewarp.view.fragment;

import android.os.Bundle;

import com.master.timewarp.R;
import com.master.timewarp.base.BaseFragment;
import com.master.timewarp.databinding.FragmentGalleryBinding;
import com.master.timewarp.view.adapter.ViewPagerGalleryAdapter;

public class GalleryFragment extends BaseFragment<FragmentGalleryBinding> {

    public static GalleryFragment newInstance() {
        Bundle args = new Bundle();
        GalleryFragment fragment = new GalleryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_gallery;
    }

    @Override
    protected void initView() {
        binding.btGalleryImage.setSelected(true);
        binding.viewPager.setPagingEnabled(false);
        ViewPagerGalleryAdapter viewPagerAdapter = new ViewPagerGalleryAdapter(getChildFragmentManager());
        binding.viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    protected void addAction() {
        binding.btGalleryImage.setOnClickListener(v -> {
            binding.btGalleryImage.setSelected(true);
            binding.btGalleryVideo.setSelected(false);
            binding.viewPager.setCurrentItem(0);
        });

        binding.btGalleryVideo.setOnClickListener(v -> {
            binding.btGalleryImage.setSelected(false);
            binding.btGalleryVideo.setSelected(true);
            binding.viewPager.setCurrentItem(1);
        });
    }
}