package com.master.timewarp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.viewpager.widget.ViewPager;

import com.master.timewarp.R;
import com.master.timewarp.view.adapter.ViewPagerMainAdapter;
import com.master.timewarp.base.BaseActivity;
import com.master.timewarp.databinding.ActivityMainBinding;
import com.master.timewarp.view.dialog.PermissionDialog;

public class MainActivity extends BaseActivity<ActivityMainBinding> {
    private ViewPagerMainAdapter viewPagerAdapter;

    public static void start(Context context) {
        Intent starter = new Intent(context, MainActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        binding.btTrending.setSelected(true);
        binding.viewPager.setPagingEnabled(false);
        viewPagerAdapter = new ViewPagerMainAdapter(getSupportFragmentManager());
        binding.viewPager.setAdapter(viewPagerAdapter);

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                binding.frSplash.setVisibility(View.GONE);
//            }
//        }, 3000);
    }

    @Override
    protected void addAction() {
        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                String title = viewPagerAdapter.getPageTitle(position).toString();
                binding.tvTitle.setText(title);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        binding.btTrending.setOnClickListener(v -> {
            binding.btTrending.setSelected(true);
            binding.btGallery.setSelected(false);
            binding.viewPager.setCurrentItem(0);
        });
        binding.btGallery.setOnClickListener(v -> {
            binding.btTrending.setSelected(false);
            binding.btGallery.setSelected(true);
            binding.viewPager.setCurrentItem(1);
        });
        binding.btCamera.setOnClickListener(v -> {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                if (checkSelfPermission(PERMISSIONS[0]) != PackageManager.PERMISSION_GRANTED
//                        || checkSelfPermission(PERMISSIONS[1]) != PackageManager.PERMISSION_GRANTED) {
//                    requestPermissions(PERMISSIONS, 1);
//                } else {
//                    CameraScannerActivity.start(this);
//                }
//            } else {

//            }
            PermissionDialog.newInstance().setCallback(new PermissionDialog.PermissionCallback() {
                @Override
                public void onPermissionGranted() {
                    CameraScannerActivity.start(MainActivity.this);
                }

                @Override
                public void onPermissionDenied() {
                    Toast.makeText(MainActivity.this, "Permission is denied!", Toast.LENGTH_SHORT).show();
                }
            }).showDialog(this);
        });
        binding.btMenu.setOnClickListener(v -> {
            SettingActivity.start(this);
        });
        binding.btPremium.setOnClickListener(v -> {
            PremiumActivity.start(this);
        });

    }

}