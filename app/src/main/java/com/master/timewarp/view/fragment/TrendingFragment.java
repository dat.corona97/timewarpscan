package com.master.timewarp.view.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;

import com.master.timewarp.R;
import com.master.timewarp.base.BaseActivity;
import com.master.timewarp.base.BaseFragment;
import com.master.timewarp.databinding.FragmentTrendingBinding;
import com.master.timewarp.model.MediaItem;
import com.master.timewarp.utils.DownloadUtils;
import com.master.timewarp.view.activity.PreviewTrendingActivity;
import com.master.timewarp.view.adapter.MediaItemAdapter;
import com.master.timewarp.worker.PlayerTestActivity;

import java.util.ArrayList;
import java.util.List;

public class TrendingFragment extends BaseFragment<FragmentTrendingBinding> {
    private List<MediaItem> mList = new ArrayList<>();
    private MediaItemAdapter adapter;

    public static TrendingFragment newInstance() {
        Bundle args = new Bundle();
        TrendingFragment fragment = new TrendingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            DownloadUtils.restApi(true, mList -> {
                TrendingFragment.this.mList = mList;
                ((BaseActivity) context).runOnUiThread(() -> adapter.updateList(TrendingFragment.this.mList));
            });
        }
    };

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_trending;
    }

    @Override
    protected void initView() {
        binding.rvTrending.setLayoutManager(new GridLayoutManager(context, 2));
        adapter = new MediaItemAdapter(mList, context);
        adapter.setCallback(mediaItem -> {
//            Intent intent = new Intent(context, PlayerTestActivity.class);
//            intent.putExtra("VIDEO_URL", "/data/user/0/com.master.timewarp/cache/youtube/U7TbAjE-pxE.mp4");
//            startActivity(intent);
            DownloadUtils.refreshList(mList, false);
            PreviewTrendingActivity.start(context, mList, mList.indexOf(mediaItem));
        });
        binding.rvTrending.setAdapter(adapter);


        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        context.registerReceiver(receiver, filter);

    }

    @Override
    public void onResume() {
        super.onResume();
        DownloadUtils.restApi(true, mList -> {
            this.mList = mList;
            ((BaseActivity) context).runOnUiThread(() -> adapter.updateList(this.mList));
        });
    }

    @Override
    protected void addAction() {

    }
}
