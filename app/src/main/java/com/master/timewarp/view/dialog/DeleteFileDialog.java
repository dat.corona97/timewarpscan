package com.master.timewarp.view.dialog;

import android.os.Bundle;

import com.master.timewarp.R;
import com.master.timewarp.base.BaseDialog;
import com.master.timewarp.databinding.DialogDeleteBinding;

public class DeleteFileDialog extends BaseDialog<DialogDeleteBinding> {
    private ICallback callback;

    public static DeleteFileDialog newInstance(int type) {
        Bundle args = new Bundle();
        args.putInt("type", type);
        DeleteFileDialog fragment = new DeleteFileDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public void setCallback(ICallback callback) {
        this.callback = callback;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_delete;
    }

    @Override
    protected void initView() {
        int type = getArguments().getInt("type");
        if (type == 0) {
            binding.tvTitleDelete.setText(R.string.delete_photo);
        } else {
            binding.tvTitleDelete.setText(R.string.delete_video);
        }
    }

    @Override
    protected void addAction() {
        binding.btCancel.setOnClickListener(v -> {
            callback.onCancel();
            dismiss();
        });
        binding.btDelete.setOnClickListener(v -> {
            callback.onDelete();
            dismiss();
        });
    }

    public interface ICallback {
        void onDelete();

        void onCancel();
    }
}
