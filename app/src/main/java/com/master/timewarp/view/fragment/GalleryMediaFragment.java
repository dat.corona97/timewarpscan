package com.master.timewarp.view.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;

import com.master.timewarp.R;
import com.master.timewarp.view.adapter.MediaItemAdapter;
import com.master.timewarp.base.BaseActivity;
import com.master.timewarp.base.BaseFragment;
import com.master.timewarp.databinding.FragmentGalleryMediaBinding;
import com.master.timewarp.model.MediaItem;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GalleryMediaFragment extends BaseFragment<FragmentGalleryMediaBinding> {
    private final List<MediaItem> mList = new ArrayList<>();
    private int type;
    private MediaItemAdapter adapter;

    public static GalleryMediaFragment newInstance(int type) {
        Bundle args = new Bundle();
        args.putInt("type", type);
        GalleryMediaFragment fragment = new GalleryMediaFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_gallery_media;
    }

    @Override
    protected void initView() {
        type = getArguments().getInt("type");
        binding.rvGallery.setLayoutManager(new GridLayoutManager(context, 2));
        adapter = new MediaItemAdapter(mList, context);
        binding.rvGallery.setAdapter(adapter);
    }


    @Override
    public void onResume() {
        super.onResume();
        mList.clear();
        if (type == 0) {
            loadImageCache();
        } else {
            loadVideoCache();
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private void loadImageCache() {
        new Thread(() -> {
            File file = new File(context.getFilesDir(), "image");
            try {
                for (File listFile : file.listFiles()) {
                    mList.add(new MediaItem(listFile.getPath(), type));
                }
                Collections.reverse(mList);
            } catch (Exception e) {
                e.printStackTrace();
            }
            ((BaseActivity) context).runOnUiThread(this::updateUI);
        }).start();
    }

    @SuppressLint("NotifyDataSetChanged")
    private void updateUI() {
        adapter.notifyDataSetChanged();
        if (mList.size() > 0) {
            binding.llEmpty.setVisibility(View.GONE);
        } else {
            binding.llEmpty.setVisibility(View.VISIBLE);
            if (type == 0) {
                binding.ivThumbEmpty.setImageResource(R.drawable.ic_empty_photo);
                binding.tvContentEmpty.setText(R.string.text_empty_photo);
            } else {
                binding.ivThumbEmpty.setImageResource(R.drawable.ic_empty_video);
                binding.tvContentEmpty.setText(R.string.text_empty_video);
            }
        }
    }

    private void loadVideoCache() {
        new Thread(() -> {
            File file = new File(context.getFilesDir(), "video");
            try {
                for (File listFile : file.listFiles()) {
                    mList.add(new MediaItem(listFile.getPath(), type));
                }
                Collections.reverse(mList);
            } catch (Exception e) {
                e.printStackTrace();
            }
            ((BaseActivity) context).runOnUiThread(this::updateUI);
        }).start();
    }

    @Override
    protected void addAction() {

    }

}
