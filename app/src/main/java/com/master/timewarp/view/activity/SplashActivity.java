package com.master.timewarp.view.activity;

import android.annotation.SuppressLint;
import android.os.Handler;

import com.master.timewarp.R;
import com.master.timewarp.base.BaseActivity;
import com.master.timewarp.databinding.ActivitySplashBinding;
import com.master.timewarp.utils.DownloadUtils;
import com.master.timewarp.utils.SharePrefUtil;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends BaseActivity<ActivitySplashBinding> {
    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initView() {
        setLanguage(SharePrefUtil.getInstance().getCurrentCode());
        DownloadUtils.restApi(true, null);
        new Handler().postDelayed(() -> {
            MainActivity.start(SplashActivity.this);
            finish();
        }, 8000);
    }

    @Override
    protected void addAction() {

    }
}
