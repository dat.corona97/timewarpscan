package com.master.timewarp.view.fragment;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.MediaController;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.kiulian.downloader.YoutubeDownloader;
import com.github.kiulian.downloader.downloader.YoutubeCallback;
import com.github.kiulian.downloader.downloader.request.RequestVideoFileDownload;
import com.github.kiulian.downloader.downloader.request.RequestVideoInfo;
import com.github.kiulian.downloader.downloader.response.Response;
import com.github.kiulian.downloader.model.videos.VideoInfo;
import com.github.kiulian.downloader.model.videos.formats.Format;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.util.Util;
import com.master.timewarp.R;
import com.master.timewarp.TimeWarpApplication;
import com.master.timewarp.base.BaseFragment;
import com.master.timewarp.databinding.FragmentPreviewTrendingBinding;
import com.master.timewarp.model.MediaItem;
import com.master.timewarp.utils.DownloadUtils;

import java.io.File;

import at.huber.youtubeExtractor.VideoMeta;
import at.huber.youtubeExtractor.YouTubeExtractor;
import at.huber.youtubeExtractor.YtFile;

public class PreviewTrendingFragment extends BaseFragment<FragmentPreviewTrendingBinding> {
    private MediaItem mediaItem;

    private static SimpleExoPlayer exoPlayer;

    public static PreviewTrendingFragment newInstance(MediaItem mediaItem, int index) {
        Bundle args = new Bundle();
        args.putSerializable("data", mediaItem);
        args.putInt("index", index);
        PreviewTrendingFragment fragment = new PreviewTrendingFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_preview_trending;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void initView() {
        mediaItem = (MediaItem) getArguments().getSerializable("data");
        binding.tvAuthor.setText("Credit to " + mediaItem.getAuthor() +" on Tik Tok");
        String text = mediaItem.getTitle();
        if (text.isEmpty()) {
            text = "#TimeWarpScan";
        }
        binding.tvTitle.setText(text);
        loadMedia();
    }


    @SuppressLint("StaticFieldLeak")
    private void loadMedia() {
        File file = new File(mediaItem.getPath());
        if (file.exists() && file.length() > 0) {
            initializePlayer();
            return;
        }
        new Thread(() -> DownloadUtils.downloadVideoCache(mediaItem.getYoutubeID())).start();
        new YouTubeExtractor(context) {
            @Override
            public void onExtractionComplete(@SuppressLint("StaticFieldLeak") SparseArray<YtFile> ytFiles, VideoMeta vMeta) {
                if (ytFiles != null) {
                    int size = ytFiles.size();
                    for (int i = 0; i < size; i++) {
                        YtFile ytFile = ytFiles.valueAt(i);
                        if (ytFile.getFormat().getHeight() < 720) {
                            continue;
                        }
                        Log.d("android_log", "onExtractionComplete: FPS: " + ytFile.getFormat().getFps() + "___EXT: " + ytFile.getFormat().getExt()
                                + "___Height: " + ytFile.getFormat().getHeight() + "____" + ytFile.getUrl());
//                        try {
//                            Glide.with(context).load(ytFile.getUrl()).into(binding.ivThumb);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
                        mediaItem.setPath(ytFile.getUrl());
                        initializePlayer();
                        break;
                    }
                }
            }

        }.extract(mediaItem.getYoutubeUrl());
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            exoPlayer.play();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            exoPlayer.pause();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            exoPlayer.stop();
            exoPlayer.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
        binding.videoView.setPlayer(null);

    }


    @Override
    protected void addAction() {
    }

    private void initializePlayer() {
        Log.d("TAG", "initializePlayer: " + mediaItem.getPath());
        try {
            exoPlayer.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
        DefaultHttpDataSource.Factory mHttpDataSourceFactory = new DefaultHttpDataSource.Factory()
                .setAllowCrossProtocolRedirects(true);

        CacheDataSource.Factory mCacheDataSourceFactory = new CacheDataSource.Factory()
                .setCache(TimeWarpApplication.getInstance().getCache())
                .setUpstreamDataSourceFactory(mHttpDataSourceFactory)
                .setFlags(CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);

        exoPlayer = new SimpleExoPlayer.Builder(context)
                .setMediaSourceFactory(new DefaultMediaSourceFactory(mCacheDataSourceFactory)).build();
        exoPlayer.addListener(new Player.Listener() {
            @Override
            public void onIsPlayingChanged(boolean isPlaying) {
                Player.Listener.super.onIsPlayingChanged(isPlaying);
                if (isPlaying) {
                    binding.frThumb.setVisibility(View.GONE);
                } else {
                    binding.frThumb.setVisibility(View.VISIBLE);
                }
            }
        });
//        Uri videoUri = Uri.fromFile(new File(mediaItem.getPath()));
        Uri videoUri = Uri.fromFile(new File(context.getFilesDir(), "video/U7TbAjE-pxE.mp4"));

        String playerInfo = Util.getUserAgent(context, "ExoPlayerInfo");
        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(
                context, playerInfo
        );
        videoUri = Uri.parse(mediaItem.getPath());
        MediaSource mediaSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(videoUri);

        binding.videoView.setPlayer(exoPlayer);
        exoPlayer.setPlayWhenReady(true);
        exoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
        exoPlayer.seekTo(0, 0);
        exoPlayer.setMediaSource(mediaSource, true);
        exoPlayer.prepare();

//        binding.videoView2.setOnPreparedListener(mediaPlayer -> {
//            mediaPlayer.setLooping(true);
//            float videoWidth = (((float) mediaPlayer.getVideoWidth()) / ((float) mediaPlayer.getVideoHeight())) / (((float) binding.videoView.getWidth()) / ((float) binding.videoView.getHeight()));
//            if (videoWidth >= 1.0f) {
//                binding.videoView2.setScaleX(videoWidth);
//            } else {
//                binding.videoView2.setScaleY(1.0f / videoWidth);
//            }
//            binding.frThumb.setVisibility(View.GONE);
//        });
////        binding.videoView2.setMediaController(new MediaController(this));
//        binding.videoView2.setVideoPath(mediaItem.getPath());
//        binding.videoView2.start();
    }


}
