package com.master.timewarp.view.dialog;

import android.os.Bundle;

import com.master.timewarp.R;
import com.master.timewarp.base.BaseDialog;
import com.master.timewarp.databinding.DialogDiscardBinding;

public class DiscardFileDialog extends BaseDialog<DialogDiscardBinding> {
    private ICallback callback;

    public static DiscardFileDialog newInstance(int type) {
        Bundle args = new Bundle();
        args.putInt("type", type);
        DiscardFileDialog fragment = new DiscardFileDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public void setCallback(ICallback callback) {
        this.callback = callback;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_discard;
    }

    @Override
    protected void initView() {
        int type = getArguments().getInt("type");
        if (type == 0) {
            binding.tvTitleDiscard.setText(R.string.discard_photo);
            binding.tvContentDiscard.setText(R.string.content_discard_photo);
        } else {
            binding.tvTitleDiscard.setText(R.string.discard_video);
            binding.tvContentDiscard.setText(R.string.content_discard_video);
        }
    }

    @Override
    protected void addAction() {
        binding.btDiscard.setOnClickListener(v -> {
            callback.onDiscard();
            dismiss();
        });
        binding.btSave.setOnClickListener(v -> {
            callback.onSave();
            dismiss();
        });
    }

    public interface ICallback {
        void onSave();

        void onDiscard();
    }
}
