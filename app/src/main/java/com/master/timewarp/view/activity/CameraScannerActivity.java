package com.master.timewarp.view.activity;

import static com.master.gpuv.egl.filter.GlTimeWarpFilter.LEFT_TO_RIGHT;
import static com.master.gpuv.egl.filter.GlTimeWarpFilter.TOP_TO_BOTTOM;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.opengl.GLException;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;

import com.bumptech.glide.Glide;
import com.master.gpuv.camerarecorder.CameraRecordListener;
import com.master.gpuv.camerarecorder.CaptureMode;
import com.master.gpuv.camerarecorder.GPUCameraRecorder;
import com.master.gpuv.camerarecorder.GPUCameraRecorderBuilder;
import com.master.gpuv.camerarecorder.LensFacing;
import com.master.gpuv.camerarecorder.Orient;
import com.master.gpuv.egl.filter.GlTimeWarpLineFilter;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.master.timewarp.R;
import com.master.timewarp.TimeWarpApplication;
import com.master.timewarp.base.BaseActivity;
import com.master.timewarp.databinding.ActivityCameraScanBinding;
import com.master.timewarp.model.MediaItem;
import com.master.timewarp.utils.DownloadUtils;
import com.master.timewarp.utils.SharePrefUtil;
import com.master.timewarp.view.adapter.MediaItemAdapter;
import com.master.timewarp.view.widget.CameraGLView;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.IntBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.opengles.GL10;

public class CameraScannerActivity extends BaseActivity<ActivityCameraScanBinding> {
    private static final int MAX_DURATION = 30;
    private static final int MIN_DURATION = 3;
    private boolean isCapture;
    private int orient;
    private boolean flash;
    private int duration;
    private long timeDelay;
    private Thread delayThread;
    private boolean timeDelayRunning;
    private Thread runtimeRecordThread;
    private CameraGLView sampleGLView;
    private boolean toggleClick = false;
    private int lensFacing = LensFacing.BACK.getFacing();
    private GPUCameraRecorder cameraRecorder;
    private String filepath;
    private int captureMode = CaptureMode.PHOTO.getMode();
    private long currentTime;
    private MediaItem lastMediaItem;
    private List<MediaItem> mList;
    private MediaItemAdapter adapter;
    private BottomSheetBehavior sheetBehavior;
    private boolean hasTrending;

    public static void start(Context context) {
        start(context, false);
    }

    public static void startWithTrending(Context context) {
        start(context, true);
    }

    public static void start(Context context, boolean hasTrending) {
        Intent starter = new Intent(context, CameraScannerActivity.class);
        starter.putExtra("hasTrending", hasTrending);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_camera_scan;
    }

    @Override
    protected void initView() {
        binding.seekSpeed.setMin(MIN_DURATION);
        binding.seekSpeed.setMax(MAX_DURATION);
        sheetBehavior = BottomSheetBehavior.from((binding.sheet.bottomSheet));
        hasTrending = getIntent().getBooleanExtra("hasTrending", false);
        updateTrending();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        hasTrending = intent.getBooleanExtra("hasTrending", false);
        updateTrending();
    }

    private void updateTrending() {
        if (hasTrending) {
            DownloadUtils.restApi(true, mList -> {
                this.mList = mList;
               runOnUiThread(() -> {
                    binding.sheet.rvTrending.setLayoutManager(new GridLayoutManager(this, 2));
                    adapter = new MediaItemAdapter(mList, this);
                    adapter.setCallback(mediaItem -> {
                        DownloadUtils.refreshList(mList, true);
                        PreviewTrendingActivity.start(this, mList, mList.indexOf(mediaItem));
                    });
                    binding.sheet.rvTrending.setAdapter(adapter);

                    adapter.updateList(this.mList);
                });
            });
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }
    }


    @SuppressLint("SetTextI18n")
    @Override
    protected void addAction() {
        binding.sheet.btClose.setOnClickListener(v -> {
            sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        });

        binding.btClose.setOnClickListener(v -> finish());
        binding.btPremium.setOnClickListener(v -> PremiumActivity.start(this));
        binding.popDelay.setOnClickListener(v -> {
        });
        binding.popSpeed.setOnClickListener(v -> {
        });
        binding.btNextSeek.setOnClickListener(v -> executeNextSeek());
        binding.btPreSeek.setOnClickListener(v -> executePreSeek());

        binding.seekSpeed.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                int progress = seekParams.seekBar.getProgress();
                binding.tvProgress.setText((progress) + "s");

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
                duration = seekBar.getProgress();
                SharePrefUtil.getInstance().setTimeDuration(duration);
                binding.tvTimeDuration.setText(duration + "s");
            }
        });
        binding.btFlash.setOnClickListener(v -> executeSwitchFlash());

        binding.btSwitchCam.setOnClickListener(v -> switchCam());

        binding.btTimeDelay.setOnClickListener(v -> {
            if (binding.popDelay.getVisibility() == View.VISIBLE) {
                binding.popDelay.setVisibility(View.GONE);
                return;
            }
            binding.popDelay.setVisibility(View.VISIBLE);
            binding.popSpeed.setVisibility(View.GONE);
        });

        binding.btTimeDuration.setOnClickListener(v -> {
            if (binding.popSpeed.getVisibility() == View.VISIBLE) {
                binding.popSpeed.setVisibility(View.GONE);
                return;
            }
            binding.popSpeed.setVisibility(View.VISIBLE);
            binding.popDelay.setVisibility(View.GONE);
        });

        binding.btOrient.setOnClickListener(v -> executeSwitchOrient());
        binding.root.setOnClickListener(v -> {
            binding.popDelay.setVisibility(View.GONE);
            binding.popSpeed.setVisibility(View.GONE);
        });

        binding.btGallery.setOnClickListener(v -> {
            binding.root.performClick();
            PreviewActivity.startFromCamera(this, lastMediaItem);
        });
        binding.btStartWarp.setOnClickListener(v -> startWarp());
        binding.btSwitchType.setOnClickListener(v -> {
            binding.root.performClick();
            if (this.captureMode == CaptureMode.VIDEO.getMode()) {
                captureMode = CaptureMode.PHOTO.getMode();
                binding.btStartWarp.setImageResource(R.drawable.ic_type_photo);
                binding.btSwitchType.setImageResource(R.drawable.ic_small_type_video);
            } else {
                captureMode = CaptureMode.VIDEO.getMode();
                binding.btStartWarp.setImageResource(R.drawable.ic_type_video);
                binding.btSwitchType.setImageResource(R.drawable.ic_small_type_photo);
            }
            SharePrefUtil.getInstance().setCaptureMode(captureMode);
            fetchLastGallery();
        });


        binding.btDelay3s.setOnClickListener(v -> executeDelayTime(3));
        binding.btDelay5s.setOnClickListener(v -> executeDelayTime(5));
        binding.btDelay10s.setOnClickListener(v -> executeDelayTime(10));
        binding.btDelay15s.setOnClickListener(v -> executeDelayTime(15));

    }

    @SuppressLint("SetTextI18n")
    private void executePreSeek() {
        duration--;
        if (duration <= MIN_DURATION) {
            duration = MIN_DURATION;
        }
        binding.seekSpeed.setProgress(duration);
        SharePrefUtil.getInstance().setTimeDuration(duration);
        binding.tvTimeDuration.setText(duration + "s");
    }

    private void executeSwitchFlash() {
        binding.root.performClick();
        if (cameraRecorder != null && cameraRecorder.isFlashSupport()) {
            cameraRecorder.switchFlashMode();
            cameraRecorder.changeAutoFocus();
        }
        flash = !flash;
        binding.btFlash.setSelected(flash && cameraRecorder != null && cameraRecorder.isFlashSupport());
    }

    private void executeSwitchOrient() {
        binding.root.performClick();
        if (orient == Orient.VERTICAL.getOrient()) {
            orient = Orient.HORIZONTAL.getOrient();
            binding.btOrient.setImageResource(R.drawable.ic_orient_horizontal);
        } else {
            orient = Orient.VERTICAL.getOrient();
            binding.btOrient.setImageResource(R.drawable.ic_orient_vertical);
        }
        SharePrefUtil.getInstance().setOrient(orient);
    }

    private void switchCam() {
        binding.root.performClick();
        releaseCamera();
        if (lensFacing == LensFacing.BACK.getFacing()) {
            lensFacing = LensFacing.FRONT.getFacing();
        } else {
            lensFacing = LensFacing.BACK.getFacing();
        }
        toggleClick = true;
        SharePrefUtil.getInstance().setLensFacing(lensFacing);
    }

    @SuppressLint("SetTextI18n")
    private void executeNextSeek() {
        duration++;
        if (duration > MAX_DURATION) {
            duration = MAX_DURATION;
        }
        binding.seekSpeed.setProgress(duration);
        SharePrefUtil.getInstance().setTimeDuration(duration);
        binding.tvTimeDuration.setText(duration + "s");
    }

    @SuppressLint("SetTextI18n")
    private void startWarp() {
        if (timeDelayRunning) {
            return;
        }
        long time = timeDelay;
        if (time > 0) {
            binding.tvDelayCountDown.setVisibility(View.VISIBLE);
            delayThread = new Thread(() -> {
                int t = 0;
                timeDelayRunning = true;
                while (t < time) {
                    try {
                        int finalT = t;
                        runOnUiThread(() -> binding.tvDelayCountDown.setText((time - finalT) + ""));
                        Thread.sleep(1000);
                        t++;
                    } catch (InterruptedException e) {
                        timeDelayRunning = false;
                        e.printStackTrace();
                        return;
                    }
                }
                runOnUiThread(this::startCapture);
            });
            delayThread.start();
            return;
        }
        startCapture();
        binding.root.performClick();
    }

    @SuppressLint("SetTextI18n")
    private void executeDelayTime(int i) {
        binding.btDelay3s.setSelected(false);
        binding.btDelay5s.setSelected(false);
        binding.btDelay10s.setSelected(false);
        binding.btDelay15s.setSelected(false);

        binding.popDelay.setVisibility(View.GONE);
        if (timeDelay == i) {
            timeDelay = 0;
            SharePrefUtil.getInstance().setTimeDelay(0);
            binding.btTimeDelay.setSelected(false);
            binding.tvDelayTime.setVisibility(View.GONE);

            return;
        }

        timeDelay = i;
        if (timeDelay == 3) {
            binding.btDelay3s.setSelected(true);
        } else if (timeDelay == 5) {
            binding.btDelay5s.setSelected(true);
        } else if (timeDelay == 10) {
            binding.btDelay10s.setSelected(true);
        } else {
            binding.btDelay15s.setSelected(true);
        }
        binding.btTimeDelay.setSelected(true);
        binding.tvDelayTime.setVisibility(View.VISIBLE);
        binding.tvDelayTime.setText(i + "s");
        SharePrefUtil.getInstance().setTimeDelay(i);
    }

    private void startCapture() {
        timeDelayRunning = false;
        binding.tvDelayCountDown.setVisibility(View.GONE);

        if (runtimeRecordThread != null) {
            runtimeRecordThread.interrupt();
        }
        if (delayThread != null) {
            delayThread.interrupt();
        }

        if (cameraRecorder != null) {
            GlTimeWarpLineFilter timeWarpLineFilter = new GlTimeWarpLineFilter();
            timeWarpLineFilter.setCallback(new GlTimeWarpLineFilter.Callback() {
                @Override
                public void onEnd() {
                    new Handler().postDelayed(() -> stopCapture(), 300);
                }

                @Override
                public void onStart() {
                }

                @Override
                public void onUpdateProgress(float f) {

                }
            });
            timeWarpLineFilter.setTypeScan((orient == Orient.VERTICAL.getOrient()) ? TOP_TO_BOTTOM : LEFT_TO_RIGHT);
            timeWarpLineFilter.setDuration(duration * 1000);
            cameraRecorder.setFilter(timeWarpLineFilter);
            hideBeforeCaptureUI();
            isCapture = true;
            if (captureMode == CaptureMode.VIDEO.getMode()) {
                File f = new File(getFilesDir(), "video");
                if (!f.exists()) {
                    f.mkdirs();
                }
                File file = new File(f, "video_" + System.currentTimeMillis() + ".mp4");
                filepath = file.getAbsolutePath();
                cameraRecorder.start(filepath);
                binding.tvTimeRecord.setVisibility(View.VISIBLE);
                runTimeThread();
            }
        }

    }

    private void runTimeThread() {
        currentTime = 0;
        runtimeRecordThread = new Thread(() -> {
            while (true) {
                try {
                    String timeText = formatTime(currentTime);
                    runOnUiThread(() -> binding.tvTimeRecord.setText(timeText));
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
                currentTime += 1000;
            }
        });
        runtimeRecordThread.start();

    }

    private String formatTime(long duration) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date(duration);
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(date);
    }

    private void stopCapture() {
        isCapture = false;
        if (runtimeRecordThread != null) {
            runtimeRecordThread.interrupt();
        }
        if (captureMode == CaptureMode.VIDEO.getMode()) {
            cameraRecorder.stop();
            PreviewActivity.startFromScan(this, new MediaItem(filepath, 1));
        } else {
            captureBitmap(bitmap -> {
                File f = new File(getFilesDir(), "image");
                if (!f.exists()) {
                    f.mkdirs();
                }
                File file = new File(f, "image_" + System.currentTimeMillis() + ".png");
                new Handler().post(() -> {
                    String imagePath = file.getAbsolutePath();
                    saveAsPngImage(bitmap, imagePath);
                    PreviewActivity.startFromScan(this, new MediaItem(imagePath, 0));
                });
            });

        }
    }

    private void hideBeforeCaptureUI() {
        binding.beforeCapture.setVisibility(View.INVISIBLE);
    }

    private void showBeforeCaptureUI() {
        binding.beforeCapture.setVisibility(View.VISIBLE);
        binding.tvTimeRecord.setVisibility(View.GONE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUI();
        setUpCamera();
        fetchLastGallery();
    }

    private void fetchLastGallery() {
        lastMediaItem = null;
        new Thread(() -> {
            List<MediaItem> list = TimeWarpApplication.getInstance().getRoomDatabase().galleryDao().getList();
            String ext = captureMode == CaptureMode.PHOTO.getMode() ? ".png" : ".mp4";
            for (int i = 0; i < list.size(); i++) {
                MediaItem mediaItem = list.get(i);
                if (new File(mediaItem.getPath()).exists() && mediaItem.getPath().endsWith(ext)) {
                    lastMediaItem = mediaItem;
                    break;
                }
            }
            runOnUiThread(() -> {
                if (lastMediaItem != null) {
                    binding.btGallery.setVisibility(View.VISIBLE);
                    try {
                        Glide.with(CameraScannerActivity.this).load(lastMediaItem.getPath()).into(binding.btGallery);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    binding.btGallery.setVisibility(View.INVISIBLE);
                }
            });
        }).start();
    }

    @SuppressLint("SetTextI18n")
    private void updateUI() {
        flash = SharePrefUtil.getInstance().isFlash();
        duration = SharePrefUtil.getInstance().getTimeDuration();
        timeDelay = SharePrefUtil.getInstance().getTimeDelay();
//        captureMode = SharePrefUtil.getInstance().getCaptureMode();
        lensFacing = SharePrefUtil.getInstance().getLensFacing();
        orient = SharePrefUtil.getInstance().getOrient();
        executeDelayTime((int) (timeDelay / 1000));
        binding.tvTimeDuration.setText(duration + "s");
        binding.tvProgress.setText(duration + "s");
        binding.seekSpeed.setProgress(duration);

        binding.btFlash.setSelected(false);
        if (this.captureMode == CaptureMode.PHOTO.getMode()) {
            binding.btStartWarp.setImageResource(R.drawable.ic_type_photo);
            binding.btSwitchType.setImageResource(R.drawable.ic_small_type_video);
        } else {
            binding.btStartWarp.setImageResource(R.drawable.ic_type_video);
            binding.btSwitchType.setImageResource(R.drawable.ic_small_type_photo);
        }

        if (orient == Orient.VERTICAL.getOrient()) {
            binding.btOrient.setImageResource(R.drawable.ic_orient_vertical);
        } else {
            binding.btOrient.setImageResource(R.drawable.ic_orient_horizontal);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        releaseCamera();
    }

    private void releaseCamera() {
        if (delayThread != null) {
            delayThread.interrupt();
            timeDelayRunning = false;
            binding.tvDelayCountDown.setVisibility(View.GONE);
        }
        if (sampleGLView != null) {
            sampleGLView.onPause();
        }

        if (cameraRecorder != null) {
            cameraRecorder.stop();
            cameraRecorder.release();
            cameraRecorder = null;
        }

        if (sampleGLView != null) {
            binding.previewView.removeView(sampleGLView);
            sampleGLView = null;
        }
        showBeforeCaptureUI();
    }


    private void setUpCameraView() {
        runOnUiThread(() -> {
            binding.previewView.removeAllViews();
            sampleGLView = null;
            sampleGLView = new CameraGLView(getApplicationContext());
            binding.previewView.addView(sampleGLView);
        });
    }

    @Override
    public void onBackPressed() {
        if (isCapture) {
            showBeforeCaptureUI();
            releaseCamera();
            setUpCamera();
            isCapture = false;
            return;
        }
        super.onBackPressed();

    }

    private void setUpCamera() {
        setUpCameraView();

        int cameraWidth = 720;
        int cameraHeight = 1280;
        int videoWidth = 720;
        int videoHeight = 1280;
        LensFacing facing = LensFacing.FRONT;
        if (lensFacing == LensFacing.BACK.getFacing()) {
            facing = LensFacing.BACK;
        }
        cameraRecorder = new GPUCameraRecorderBuilder(this, sampleGLView)
                //.recordNoFilter(true)
                .cameraRecordListener(new CameraRecordListener() {
                    @Override
                    public void onGetFlashSupport(boolean flashSupport) {
                        runOnUiThread(() -> {
                            flash = false;
                            binding.btFlash.setSelected(flash);
                        });
                    }

                    @Override
                    public void onRecordComplete() {
//                        exportMp4ToGallery(getApplicationthis(), filepath);
                    }

                    @Override
                    public void onRecordStart() {

                    }

                    @Override
                    public void onError(Exception exception) {
                        Log.e("GPUCameraRecorder", exception.toString());
                    }

                    @Override
                    public void onCameraThreadFinish() {
                        if (toggleClick) {
                            runOnUiThread(() -> setUpCamera());
                        }
                        toggleClick = false;
                    }

                    @Override
                    public void onVideoFileReady() {

                    }
                })
                .videoSize(videoWidth, videoHeight)
                .cameraSize(cameraWidth, cameraHeight)
                .lensFacing(facing)
                .mute(true)
                .build();

        cameraRecorder.changeAutoFocus();

    }

    private void captureBitmap(BitmapReadyCallbacks bitmapReadyCallbacks) {
        sampleGLView.queueEvent(() -> {
            EGL10 egl = (EGL10) EGLContext.getEGL();
            GL10 gl = (GL10) egl.eglGetCurrentContext().getGL();
            Bitmap snapshotBitmap = createBitmapFromGLSurface(sampleGLView.getMeasuredWidth(), sampleGLView.getMeasuredHeight(), gl);

            runOnUiThread(() -> bitmapReadyCallbacks.onBitmapReady(snapshotBitmap));
        });
    }

    private Bitmap createBitmapFromGLSurface(int w, int h, GL10 gl) {

        int[] bitmapBuffer = new int[w * h];
        int[] bitmapSource = new int[w * h];
        IntBuffer intBuffer = IntBuffer.wrap(bitmapBuffer);
        intBuffer.position(0);

        try {
            gl.glReadPixels(0, 0, w, h, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, intBuffer);
            int offset1, offset2, texturePixel, blue, red, pixel;
            for (int i = 0; i < h; i++) {
                offset1 = i * w;
                offset2 = (h - i - 1) * w;
                for (int j = 0; j < w; j++) {
                    texturePixel = bitmapBuffer[offset1 + j];
                    blue = (texturePixel >> 16) & 0xff;
                    red = (texturePixel << 16) & 0x00ff0000;
                    pixel = (texturePixel & 0xff00ff00) | red | blue;
                    bitmapSource[offset2 + j] = pixel;
                }
            }
        } catch (GLException e) {
            Log.e("CreateBitmap", "createBitmapFromGLSurface: " + e.getMessage(), e);
            return null;
        }

        return Bitmap.createBitmap(bitmapSource, w, h, Bitmap.Config.ARGB_8888);
    }

    public void saveAsPngImage(Bitmap bitmap, String filePath) {
        try {
            File file = new File(filePath);
            FileOutputStream outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private interface BitmapReadyCallbacks {
        void onBitmapReady(Bitmap bitmap);
    }

}
