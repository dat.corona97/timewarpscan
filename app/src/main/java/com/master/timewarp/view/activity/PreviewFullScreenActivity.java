package com.master.timewarp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.MediaController;

import com.bumptech.glide.Glide;
import com.master.timewarp.R;
import com.master.timewarp.base.BaseActivity;
import com.master.timewarp.databinding.ActivityPreviewFullscreenBinding;
import com.master.timewarp.model.MediaItem;

public class PreviewFullScreenActivity extends BaseActivity<ActivityPreviewFullscreenBinding> {
    private MediaItem mediaItem;

    public static void start(Context context, MediaItem mediaItem) {
        Intent starter = new Intent(context, PreviewFullScreenActivity.class);
        starter.putExtra("data", mediaItem);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_preview_fullscreen;
    }

    @Override
    protected void initView() {
        mediaItem = (MediaItem) getIntent().getSerializableExtra("data");
        try {
            Glide.with(this).load(mediaItem.getPath()).into(binding.ivThumb);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mediaItem.getType() == 1) {
            playVideo();
        } else {
            binding.ivPlayIcon.setVisibility(View.GONE);
//            binding.btCloseFullscreen.setVisibility(View.GONE);
        }
    }

    private void playVideo() {
        binding.frThumb.setVisibility(View.GONE);
        binding.videoView.setOnPreparedListener(mediaPlayer -> {
            mediaPlayer.setLooping(true);
            float videoWidth = (((float) mediaPlayer.getVideoWidth()) / ((float) mediaPlayer.getVideoHeight())) / (((float) binding.videoView.getWidth()) / ((float) binding.videoView.getHeight()));
            if (videoWidth >= 1.0f) {
                binding.videoView.setScaleX(videoWidth);
            } else {
                binding.videoView.setScaleY(1.0f / videoWidth);
            }
        });
        binding.videoView.setMediaController(new MediaController(this));
        binding.videoView.setVideoPath(mediaItem.getPath());
        binding.videoView.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        binding.frThumb.setVisibility(View.VISIBLE);
    }

    @Override
    protected void addAction() {
        binding.frThumb.setOnClickListener(v -> {
            if (mediaItem.getType() == 1) {
                playVideo();
            }
        });
        binding.btCloseFullscreen.setOnClickListener(v -> finish());
    }
}
