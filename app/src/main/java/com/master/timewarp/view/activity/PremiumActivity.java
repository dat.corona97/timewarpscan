package com.master.timewarp.view.activity;

import android.content.Context;
import android.content.Intent;

import com.master.timewarp.R;
import com.master.timewarp.base.BaseActivity;
import com.master.timewarp.databinding.ActivityPremiumBinding;

public class PremiumActivity extends BaseActivity<ActivityPremiumBinding> {
    public static void start(Context context) {
        Intent starter = new Intent(context, PremiumActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_premium;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void addAction() {

    }
}
