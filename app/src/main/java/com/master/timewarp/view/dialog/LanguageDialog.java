package com.master.timewarp.view.dialog;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.master.timewarp.R;
import com.master.timewarp.base.BaseDialog;
import com.master.timewarp.databinding.DialogLanguageBinding;
import com.master.timewarp.model.ItemLanguage;
import com.master.timewarp.utils.SharePrefUtil;
import com.master.timewarp.view.adapter.LanguageAdapter;

import java.util.ArrayList;
import java.util.List;

public class LanguageDialog extends BaseDialog<DialogLanguageBinding> {
    private List<ItemLanguage> mList = new ArrayList<>();
    private LanguageAdapter languageAdapter;


    public static LanguageDialog newInstance() {

        Bundle args = new Bundle();
        LanguageDialog fragment = new LanguageDialog();
        fragment.setArguments(args);
        return fragment;
    }

    private ICallback callback;

    public void setCallback(ICallback callback) {
        this.callback = callback;
    }

    public interface ICallback {
        void submitLanguage(ItemLanguage itemLanguage);
    }


    private static List<ItemLanguage> getListCountry() {
        List<ItemLanguage> mList = new ArrayList<>();
        mList.add(new ItemLanguage(0, "English", R.drawable.ic_disable, "en"));
        mList.add(new ItemLanguage(0, "Arabic", R.drawable.ic_disable, "ar"));
        mList.add(new ItemLanguage(0, "Bulgarian", R.drawable.ic_disable, "bg"));
        mList.add(new ItemLanguage(0, "Chinese (Simplified)", R.drawable.ic_disable, "zh"));
        mList.add(new ItemLanguage(0, "Chinese (Traditional)", R.drawable.ic_disable, "zh"));
        mList.add(new ItemLanguage(0, "Czech", R.drawable.ic_disable, "cs"));
        mList.add(new ItemLanguage(0, "Polish", R.drawable.ic_disable, "pl"));
        mList.add(new ItemLanguage(0, "Dutch", R.drawable.ic_disable, "ru"));
        mList.add(new ItemLanguage(0, "French (France)", R.drawable.ic_disable, "fr"));
        mList.add(new ItemLanguage(0, "German", R.drawable.ic_disable, "de"));
        mList.add(new ItemLanguage(0, "Greek", R.drawable.ic_disable, "el"));
        mList.add(new ItemLanguage(0, "Hindi", R.drawable.ic_disable, "hi"));
        mList.add(new ItemLanguage(0, "Italian", R.drawable.ic_disable, "it"));
        mList.add(new ItemLanguage(0, "Indonesian", R.drawable.ic_disable, "in"));
        mList.add(new ItemLanguage(0, "Korean", R.drawable.ic_disable, "ko"));
        mList.add(new ItemLanguage(0, "Russian", R.drawable.ic_disable, "ru"));
        mList.add(new ItemLanguage(0, "Romanian", R.drawable.ic_disable, "ro"));
        mList.add(new ItemLanguage(0, "Swedish", R.drawable.ic_disable, "sv"));
        mList.add(new ItemLanguage(0, "Spanish (Spain)", R.drawable.ic_disable, "es"));
        mList.add(new ItemLanguage(0, "Thai", R.drawable.ic_disable, "th"));
        mList.add(new ItemLanguage(0, "Portuguese (Portugal)", R.drawable.ic_disable, "pt"));
        mList.add(new ItemLanguage(0, "Vietnamese", R.drawable.ic_disable, "vi"));
        return mList;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_language;
    }

    @Override
    protected void initView() {
        initListLanguage();
        initRCLanguage();
    }

    @Override
    protected void addAction() {
        binding.btSubmit.setOnClickListener(v -> {
            if (itemLanguage != null) {
                SharePrefUtil.getInstance().setCurrentCode(itemLanguage.getLanguageToLoad());
                callback.submitLanguage(itemLanguage);
            }
            dismiss();
        });
    }

    private ItemLanguage itemLanguage;

    @SuppressLint("NotifyDataSetChanged")
    private void initRCLanguage() {
        languageAdapter = new LanguageAdapter(mList, getContext());
        binding.rcLanguage.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rcLanguage.setAdapter(languageAdapter);
        languageAdapter.setLanguageCallback(itemLanguage -> {
            LanguageDialog.this.itemLanguage = itemLanguage;
            for (ItemLanguage item : mList) {
                item.setImgSelect(R.drawable.ic_disable);
            }
            itemLanguage.setImgSelect(R.drawable.ic_checked);
            languageAdapter.notifyDataSetChanged();
        });
    }

    private void initListLanguage() {
        mList = getListCountry();
        String currentCode = SharePrefUtil.getInstance().getCurrentCode();
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).getLanguageToLoad().equals(currentCode)) {
                mList.get(i).setImgSelect(R.drawable.ic_checked);
                return;
            }
        }
    }

}
