package com.master.timewarp.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.master.gpuv.camerarecorder.CaptureMode;
import com.master.gpuv.camerarecorder.LensFacing;
import com.master.gpuv.camerarecorder.Orient;
import com.master.timewarp.TimeWarpApplication;

public class SharePrefUtil {
    @SuppressLint("StaticFieldLeak")
    private static SharePrefUtil instance;
    private final Context context;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private SharePrefUtil() {
        context = TimeWarpApplication.getInstance();
        init();
    }

    public static SharePrefUtil getInstance() {
        if (instance == null) {
            instance = new SharePrefUtil();
        }
        return instance;
    }

    public void init() {
        preferences = context.getSharedPreferences("pref_setting", Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public int getTimeDuration() {
        return preferences.getInt("duration", 10);
    }

    public void setTimeDuration(int duration) {
        editor.putInt("duration", duration).apply();
    }

    public long getTimeDelay() {
        return preferences.getInt("delay", 0) * 1000L;
    }

    public void setTimeDelay(int delay) {
        editor.putInt("delay", delay).apply();
    }

    public boolean isFlash() {
        return preferences.getBoolean("splash", false);
    }

    public void setFlash(boolean splash) {
        editor.putBoolean("splash", splash).apply();
    }

    public int getLensFacing() {
        return preferences.getInt("facing", LensFacing.FRONT.getFacing());
    }

    public void setLensFacing(int facing) {
        editor.putInt("facing", facing).apply();
    }

    public int getOrient() {
        return preferences.getInt("orient", Orient.VERTICAL.getOrient());
    }

    public void setOrient(int orient) {
        editor.putInt("orient", orient).apply();
    }

    public int getCaptureMode() {
        return preferences.getInt("capture_mode", CaptureMode.PHOTO.getMode());
    }

    public void setCaptureMode(int mode) {
        editor.putInt("capture_mode", mode).apply();
    }

    public void setDenyPermission(boolean b) {
        editor.putBoolean("permission_deny", b).apply();
    }

    public boolean isPermissionDeny() {
        return preferences.getBoolean("permission_deny", false);
    }

    public String getCurrentCode() {
        return preferences.getString("language_code", "en");
    }
    public void setCurrentCode(String languageCode) {
        editor.putString("language_code", languageCode).apply();
    }
}
