package com.master.timewarp.utils;

import android.annotation.SuppressLint;
import android.util.Log;

import com.github.kiulian.downloader.YoutubeDownloader;
import com.github.kiulian.downloader.downloader.YoutubeCallback;
import com.github.kiulian.downloader.downloader.request.RequestVideoFileDownload;
import com.github.kiulian.downloader.downloader.request.RequestVideoInfo;
import com.github.kiulian.downloader.downloader.response.Response;
import com.github.kiulian.downloader.model.videos.VideoInfo;
import com.github.kiulian.downloader.model.videos.formats.Format;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.master.timewarp.TimeWarpApplication;
import com.master.timewarp.model.MediaItem;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class DownloadUtils {


    @SuppressLint({"NotifyDataSetChanged", "StaticFieldLeak"})
    public static void restApi(boolean download, OnCallback callback) {
        new Thread(() -> {
            BufferedReader reader = null;
            try {
                StringBuilder rs = new StringBuilder();
                reader = new BufferedReader(
                        new InputStreamReader(TimeWarpApplication.getInstance().getAssets().open("data.json")));
                String mLine;
                while ((mLine = reader.readLine()) != null) {
                    rs.append(mLine);
                }
                List<MediaItem> mList = new Gson().fromJson(rs.toString(), new TypeToken<List<MediaItem>>() {
                }.getType());
                refreshList(mList, download);

                if (callback != null) {
                    callback.updateUI(mList);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        //log the exception
                    }
                }
            }
        }).start();
    }

    public interface OnCallback {
        void updateUI(List<MediaItem> mList);
    }

    @SuppressLint({"NotifyDataSetChanged", "StaticFieldLeak"})
    public static void refreshList(List<MediaItem> mList, boolean download) {
        for (int i = 0; i < mList.size(); i++) {
            MediaItem mediaItem = mList.get(i);
            mediaItem.setType(1);
            File file = new File(TimeWarpApplication.getInstance().getFilesDir() + "/youtube", mediaItem.getYoutubeID() + ".mp4");
            if (file.exists() && file.length() > 0) {
                mediaItem.setPath(file.getAbsolutePath());
            } else {
                mediaItem.setPath("https://i.ytimg.com/vi/" + mediaItem.getYoutubeID() + "/maxresdefault.jpg");
                if (download) {
                    new Thread(() -> DownloadUtils.downloadVideoCache(mediaItem.getYoutubeID())).start();
                }
            }
        }
    }

    public static void downloadVideoCache(String videoId) {
        File file = new File(TimeWarpApplication.getInstance().getFilesDir() + "/youtube", videoId + ".mp4");
        if (file.exists() && file.length() > 0) {
            return;
        }
        Log.d("android_log", "downloadVideoCache: " + videoId);
        RequestVideoInfo request = new RequestVideoInfo(videoId)
                .callback(new YoutubeCallback<VideoInfo>() {
                    @Override
                    public void onFinished(VideoInfo videoInfo) {
                        System.out.println("Finished parsing");
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        System.out.println("Error: " + throwable.getMessage());
                    }
                })

                .async();
        YoutubeDownloader downloader = new YoutubeDownloader();
        Response<VideoInfo> response = downloader.getVideoInfo(request);
        VideoInfo video = response.data();
        if (video == null) {
            return;
        }
        System.out.println(video.details().thumbnails());


        File outputDir = new File(TimeWarpApplication.getInstance().getFilesDir(), "youtube");
        Format format = null;
        try {
            format = video.videoFormats().get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < video.videoFormats().size(); i++) {
            if (video.videoFormats().get(i).videoQuality().name().equals("large")) {
                format = video.videoFormats().get(i);
                break;
            }
        }
        if (format == null) {
            return;
        }
        // sync downloading
        RequestVideoFileDownload requestDownload = new RequestVideoFileDownload(format)
                .saveTo(outputDir) // by default "videos" directory
                .renameTo(videoId) // by default file name will be same as video title on youtube
                .overwriteIfExists(true); // if false and file with such name already exits sufix will be added video(1).mp4
        downloader.downloadVideoFile(requestDownload);
    }

}
