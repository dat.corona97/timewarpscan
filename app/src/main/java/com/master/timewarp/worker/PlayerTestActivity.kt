package com.master.timewarp.worker

import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.upstream.cache.CacheDataSource
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import com.google.android.exoplayer2.util.Util
import com.master.timewarp.TimeWarpApplication
import com.master.timewarp.databinding.ActivityPlayerBinding
import java.io.File


class PlayerTestActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPlayerBinding

    private lateinit var mHttpDataSourceFactory: HttpDataSource.Factory
    private lateinit var mDefaultDataSourceFactory: DefaultDataSourceFactory
    private lateinit var mCacheDataSourceFactory: DataSource.Factory
    private lateinit var exoPlayer: SimpleExoPlayer
    private val cache: SimpleCache = TimeWarpApplication.getInstance().cache

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlayerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val videoUrl = intent.getStringExtra("VIDEO_URL")

        mHttpDataSourceFactory = DefaultHttpDataSource.Factory()
            .setAllowCrossProtocolRedirects(true)

        this.mDefaultDataSourceFactory = DefaultDataSourceFactory(
            applicationContext, mHttpDataSourceFactory
        )

        mCacheDataSourceFactory = CacheDataSource.Factory()
            .setCache(cache)
            .setUpstreamDataSourceFactory(mHttpDataSourceFactory)
            .setFlags(CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR)

        exoPlayer = SimpleExoPlayer.Builder(applicationContext)
            .setMediaSourceFactory(DefaultMediaSourceFactory(mCacheDataSourceFactory)).build()

//        val videoUri = Uri.parse(File(filesDir, "video/U7TbAjE-pxE.mp4").path)
//        val mediaItem = MediaItem.fromUri(videoUri)
//        val mediaSource =
//            ProgressiveMediaSource.Factory(mCacheDataSourceFactory).createMediaSource(mediaItem)
////
//        binding.playerView.player = exoPlayer
//        exoPlayer.playWhenReady = true
//        exoPlayer.seekTo(0, 0)
//        exoPlayer.setMediaSource(mediaSource, true)
//        exoPlayer.prepare()

//
//        exoPlayer = ExoPlayerFactory.newSimpleInstance(
//            this,
//            DefaultTrackSelector(null),
//            DefaultLoadControl()
//        )
//        exoPlayer.addListener(eventListener)
//
//        val dataSpec = DataSpec(uri)
//        val fileDataSource = FileDataSource()
//        try {
//            fileDataSource.open(dataSpec)
//        } catch (e: FileDataSource.FileDataSourceException) {
//            e.printStackTrace()
//        }
//
//        val factory = DataSource.Factory { fileDataSource }
//        val audioSource: MediaSource = ExtractorMediaSource(
//            fileDataSource.getUri(),
//            factory, DefaultExtractorsFactory(), null, null
//        )
//
//        exoPlayer.prepare(audioSource)
        val videoUri = Uri.fromFile(File(filesDir, "video/U7TbAjE-pxE.mp4"))

        val playerInfo: String = Util.getUserAgent(this, "ExoPlayerInfo")
        val dataSourceFactory = DefaultDataSourceFactory(
            this, playerInfo
        )
        val mediaSource =
            ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(videoUri)

//        val mediaSource: MediaSource = ProgressiveMediaSource.Factory(dataSourceFactory)
//            .setExtractorsFactory(DefaultExtractorsFactory())
//            .createMediaSource(videoUri)
        exoPlayer.playWhenReady = true
        exoPlayer.seekTo(0, 0)
        exoPlayer.setMediaSource(mediaSource, true)
        exoPlayer.prepare()
        binding.playerView.player = exoPlayer

    }

}